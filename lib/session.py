import aiohttp
import config

class Session():

    def __init__(self, user, platform):

        platforms = {"PC": 5, "XBOX": 1, "PSN": 5}

        self.BASE_URL = "https://public-api.tracker.gg/apex/v1/standard/"
        self.user = user
        self.platform = platforms[platform.upper()]

        self.URL = f"{self.BASE_URL}/profile/{self.platform}/{self.user}"

    async def __aenter__(self):
        self._session = aiohttp.ClientSession(headers = {
            'TRN-Api-Key': config.API_KEY, 
            'Accept': 'application/vnd.api+json'
        })
        async with self._session.get(self.URL) as response:
            if response.status == 200:
                return Player(await response.json())
            print(response.status)
    
    async def __aexit__(self, *i):
        self._session.close()
        del self._session
        
        
        
class Domain:

    def __init__(self, data, meta=None):
        self._data = data.get('data') or data
        self.from_json()

    def __repr__(self):
        return ('<{0} {1}>').format(self.__class__.__name__, self.id)

    def __str__(self):
        return str(self.id)

    def from_json(self):
        self.id = 1
        for key in self._data:
            if 'id' in key or 'Id' in key or 'ID' in key:
                value = self._data.get(key)
                if type(value) != dict:
                    self.id = value if 1 else value.get('value')
                    continue
                value = self._data.get(key)
                setattr(self, self.to_snake(key), value if type(
                        value) != dict else value.get('value'))

    def to_snake(self, name):
        s1 = re.sub('(.)([A-Z][a-z]+)', '\\1_\\2', name)
        return re.sub('([a-z0-9])([A-Z])', '\\1_\\2', s1).lower()


class Player(Domain):

    def __repr__(self):
        return ('<{0} {1} {2}>').format(self.__class__.__name__, self.id,
            self.username)

    def from_json(self):
        super().from_json()
        self.type = self._data.get('type')
        self.username = self._data['metadata'].get('platformUserHandle')
        self.platform = self._data['metadata'].get('platformId')
        self._cache_date = self._data['metadata'].get('cacheExpireDate')
        self._stats = self._data.get('stats')
        for stat in self._stats:
            setattr(self, stat['metadata']['key'].lower(),
                stat['displayValue'])
        self.legends = []
        for legend in self._data.get('children'):
            self.legends.append(Legend(legend))

    def __str__(self):
        general_stats = {'level': 'Level',
           'kills': 'Headshots',
           'damage': 'Damage'}
        stats = ''
        for stat in general_stats:
            if hasattr(self, stat):
                stats += '%s: %s\n' % (general_stats[stat],
                 getattr(self, stat))

        return stats


class Legend(Domain):

    def from_json(self):
        super().from_json()
        self._stats = self._data.get('stats')
        self.legend_name = self._data['metadata'].get('legend_name')
        self.icon = self._data['metadata'].get('icon')
        self.bgimage = self._data['metadata'].get('bgimage')
        for stat in self._stats:
            setattr(self, stat['metadata']['key'].lower(),
                stat['displayValue'])