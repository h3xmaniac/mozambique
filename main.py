import discord
from discord.ext import commands
import asyncio
import uvloop
from lib.session import Session
from config import TOKEN

startup_extensions = [
    "cogs.test",
    "jishaku"  
]


asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
loop = asyncio.get_event_loop()

bot = commands.Bot(command_prefix="|", loop=loop)
bot.get = lambda *i: Session(*i)

@bot.event
async def on_ready():
    for ext in startup_extensions:
        bot.load_extension(ext)
    print("READY")
    

bot.run(TOKEN)