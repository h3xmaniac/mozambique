import discord
from discord.ext import commands


class Test(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def getprof(self, ctx, profile, platform):
        async with self.bot.get(profile,platform) as player:    
            embed = discord.Embed(name=platform)\
                .add_field(
                    name="Stats",
                    value=f"Level: **{player.level}**\nKills: **{player.kills}**"
                )
            for l in player.legends:
                embed.add_field(
                    name=l.legend_name,
                    value=f"Kills: **{l.kills}**"
                )
            await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(Test(bot))
            


                    
                
            
    
    